# 服务端部署说明

环境:  node 6.2.0, 
msyql 5.5, 

## nginx配置
```
server{

        listen 443 ssl;
        server_name orange.geinigejuzichi.top;

        root /var/www/orange/;

        ssl on;
        ssl_certificate *.pem;
        ssl_certificate_key *.pem;
        ssl_session_timeout 5m;

        location / {
                proxy_pass http://127.0.0.1:3000;
                proxy_set_header X-Real-IP $remote_addr; #原请求来源ip 真实ip最可信
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for; #http客户端或者反向代理生
         }
         
}

```