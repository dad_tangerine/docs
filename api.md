#  协议说明文档-----必读------

* 1.请求地址 [域名]/wx_api/[协议名]
    > 例如https://www.baidu.com/wx_api/getUserInfo

* 2.返回值说明

所有POST接口的返回值, 均采用以上格式, 业务数据都在 <font color=#00ffff>DATA</font>中


```javascript
    返回值中数据结构
    {
        result:0/1,
        msg:"xxxx",
        data:DATA
    }
```


## 1.获取重要配置接口

> POST => [域名]/wx_api/getConfig
>

```
返回值说明:
{
        'video_url': '',
        'orange_min': '',//最少提现的橘子数量
        'postage': '',//邮费
        'price_list': [
            {'price': 1, 'amonut': 2},
            {'price': 6, 'amonut': 15},
            {'price': 10, 'amonut': 30}
        ]

    }
```

## 2.统计接口

> POST => [域名]/wx_api/Statistics
>

```
参数说明:
{
    'action': 1/2/3/4,
    'detail':'点击了某个按钮',
    'extra': ojb
}

返回值说明:
{
       'result': 0,
       'msg': 'xxxxxxxxxx'

    }
```

> extra: 某些统计行为, 涉及到一些参数, 例如游戏id, 用户id
{
    'opeinid':'xx',
    'gameid':'xxxxx'
}



action说明

| 参数名        | 说明   |  
| --------   | -----:  | 
| 1000       | 点击按钮xx |
| 1001     | 点击按钮xx |
| .......     | ....... |

