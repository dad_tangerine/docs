##  橘子项目文档

### 一. 协议文档

#### [&emsp;&emsp;1. 协议接口总览(必读)](api.md)
#### [&emsp;&emsp;2. 用户接口](user_api.md)
#### [&emsp;&emsp;3. 游戏接口](game_api.md)
#### [&emsp;&emsp;4. 充值与提现<未定稿>](money_api.md)

### 二. 数据库设计

#### [&emsp;&emsp;1. 表结构](sql.md)


### 三. [服务端部署说明](server_install.md)
