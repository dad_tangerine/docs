# 数据库设计



````
CREATE DATABASE orange;

#创建用户表
CREATE TABLE user {
    id INT PARMARY KEY,
    openid VARCHAR(100) NOT NULL,
    unionid VARCHAR(100) NOT NULL,
    channel VARCHAR(100) NOT NULL DEFAULT "orange",
    nickName VARCHAR(100),
    avatarUrl VARCHAR(100),
    gender INT DEFAULT 1,#1:男,2:女
    addr VARCHAR(512),#地址
    balance INT DEFAULT 0,#余额,单位为分
    orange INT DEFAULT 0#账户橘子数量
}

#创建一句游戏表
CREATE TABLE round{
    id INT PARMARY KEY,
    uid INT NOT NULL,#创建游戏的人
    total INT NOT NULL,#总个数
    time TIMESTAMP DEFAULT current_timestamp,
    slogen VARCHAR(100)
}

#加入游戏表
CREATE TABLE partake{
    id INT PARMARY KEY,
    rid INT NOT NULL,
    uid INT NOT NULL,
    time TIMESTAMP DEFAULT current_timestamp #领取时间
}

#消费记录表
CREATE TABLE record{
    id INT PRIMARY KEY,
    type INT NOT NULL,# 1:充值,2:消费,3:提现,4:购买橘子,5:得到橘子
    des VARCHAR(100) NOT NULL,
    balance_orange INT NOT NULL,
    balance_money INT NOT NULL,
    time TIMESTAMP DEFAULT current_timestamp
}
````



# 以下内容废弃,仅作为记录保存


## 1.user  [用户表]
```javascript
> id
> openid
> unionid
> from           //渠道
> nickName
> avatarUrl
> gender
> add
> balance       //余额,也就是橘子个数
```

## 2.round [玩一局]
```javascript
> id
> uid
> total         //总个数
> time
> slogen        //口号 默认:爸爸给你橘子吃
```

## 3.round_trade [交易表,领取橘子或超时返还橘子]
```javascript
> id
> rid           //哪一局id
> uid           //领取人的id
> time          //领取时间
> type          //1:被领取, -1:被返还
```

## 4.充值记录 [充值表]
```javascript
> id
> uid
> amount        //单位: 分
> orange_number //单位: 个
> time
```

## 5.withdraw [提现表]
```javascript
> id
> uid
> amount    //提现数量
> add       //收货地址
```